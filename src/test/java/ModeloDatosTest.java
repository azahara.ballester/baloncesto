import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ModeloDatosTest {

    @Test
    public void testExisteJugador() {
        System.out.println("Prueba de existeJugador");
        String nombre = "";
        ModeloDatos instance = new ModeloDatos();
        instance.abrirConexion();
        boolean expResult = false;
        boolean result = instance.existeJugador(nombre);
        assertEquals(expResult, result);  
        instance.cerrarConexion();
        //fail("Fallo forzado.");
    }

    @Test
    public void testActualizarJugador() {
        System.out.println("Prueba de actualizarJugador");
        String nombre = "Rudy";
        ModeloDatos instance = new ModeloDatos();
        instance.abrirConexion();
        boolean existe = instance.existeJugador(nombre);

        if(existe){
            int votos = instance.votosJugador(nombre);
            System.out.println("----- Votos anteriores: "+ votos+" votos");
            instance.actualizarJugador(nombre);
            int expResult = votos+1;
            int result = instance.votosJugador(nombre);
            System.out.println("----- Resultado esperado tras la actualización: "+ expResult+" votos");
            System.out.println("----- Resultado obtenido tras la actualización: "+ result +" votos");
            assertEquals(expResult, result);
            System.out.println("La pueba ha concluido con exito");
            
        }
        instance.cerrarConexion();
    }
}