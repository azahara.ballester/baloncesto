<%@ page import="java.util.List" %>
<%@ page import="domain.Jugador" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="ES">
<html>
    <head><title>Votaci&oacute;n mejor jugador liga ACB</title></head>
    <body>
        <font size=10>
        Votaci&oacute;n al mejor jugador de la liga ACB
        <br>

        <p align="center"> VOTOS TOTALES</p>
        <table>
            <thead>
                <th> Nombre jugador </th>
                <th> Votos </th>
            </thead>
            <tbody>
                <%
                    List<Jugador> lista = (List<Jugador>) session.getAttribute("lista");
                    for (Jugador j : lista){
                        out.print("<tr>");
                        out.print("<td>" + j.getName() + "</td>");
                        out.print("<td>" + j.getVotes() + "</td>");
                        out.print("</tr>");
                    }
                %>
            </tbody>
        </table>
        </font>
        
        

        <br> <a href="index.html"> Ir al comienzo</a>
    </body>
</html>
