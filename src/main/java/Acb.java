
import java.io.*;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import domain.Jugador;

public class Acb extends HttpServlet {

    private ModeloDatos bd;

    @Override
    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }


    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession s = req.getSession(true);
        

        if(req.getParameter("ponerACero") != null){
            bd.ponerVotosCero();
            res.sendRedirect(res.encodeRedirectURL("index.html"));
        }
        else if(req.getParameter("verVotos") != null){
            int votos = bd.votosJugador("Rudy");
            s.setAttribute("NombreJugador", "Rudy");
            s.setAttribute("votos", votos);
            List<Jugador> lista = bd.verVotos();
            s.setAttribute("lista", lista);
            res.sendRedirect(res.encodeRedirectURL("VerVotos.jsp"));
        }
        else if(req.getParameter("votar") != null){
            String nombreP = (String) req.getParameter("txtNombre");
            String nombre = (String) req.getParameter("R1");
            if (nombre.equals("Otros")) {
                nombre = (String) req.getParameter("txtOtros");
            }
            if (bd.existeJugador(nombre)) {
                bd.actualizarJugador(nombre);
            } else {
                bd.insertarJugador(nombre);
            }
            s.setAttribute("nombreCliente", nombreP);
            // Llamada a la página jsp que nos da las gracias
            res.sendRedirect(res.encodeRedirectURL("TablaVotos.jsp"));
        }        
    }
    
    @Override
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
