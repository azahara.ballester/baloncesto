package domain;
public class Jugador{

    private final Integer id;
    private final String name;
    private final Integer votes;

    public Jugador(Integer id, String name, Integer votes){
        this.id = id;
        this.name = name;
        this.votes = votes;
    }

    public Integer getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public Integer getVotes(){
        return votes;
    }

}
